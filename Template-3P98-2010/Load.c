/* An example of how to read an image (img.tif) from file using freeimage and then
display that image using openGL's drawPixelCommand. Also allow the image to be saved
to backup.tif with freeimage and a simple thresholding filter to be applied to the image.
Conversion by Lee Rozema.
Added triangle draw routine, fixed memory leak and improved performance by Robert Flack (2008)
*/

#include <stdlib.h>
#include <math.h>

#ifdef __unix__
	#include <GL/freeglut.h>
#elif defined(_WIN32) || defined(WIN32)
	#include <freeglut.h>
#endif


#include <FreeImage.h>
#include <stdio.h>
#include <malloc.h>


//the pixel structure
typedef struct {
	GLubyte r, g, b;
} pixel;
typedef enum { false, true } bool;

//the global structure
typedef struct {
	pixel *data;
	pixel *original;
	pixel *workspace;
	int **average;
	int **sobelHor;
	int **sobelVert;
	int **OwnMatrix1;
	int **OwnMatrix2;
	int **Random;
	int **PresetColours;
	int mouse_x, mouse_y;
	int w, h, paintBrushSize, paintColour;
	bool paintOn, blend1On, blend2On, pete1, pete2;
	int startX, startY;
} glob;
glob global;

//read image
pixel *read_img(char *name, int *width, int *height) {
	FIBITMAP *image;
	int i,j,pnum;
	RGBQUAD aPixel;
	pixel *data;

	if((image = FreeImage_Load(FIF_TIFF, name, 0)) == NULL) {
		return NULL;
	}      
	*width = FreeImage_GetWidth(image);
	*height = FreeImage_GetHeight(image);

	//data = (pixel *)malloc((*height)*(*width)*sizeof(pixel *));
	data = (pixel *)malloc((*height)*(*width)*sizeof(pixel));
	pnum=0;
	for(i = 0 ; i < (*height) ; i++) {
		for(j = 0 ; j < (*width) ; j++) {
			FreeImage_GetPixelColor(image, j, i, &aPixel);
			data[pnum].r = (aPixel.rgbRed);
			data[pnum].g = (aPixel.rgbGreen);
			data[pnum++].b = (aPixel.rgbBlue);
		}
	}
	FreeImage_Unload(image);
	return data;
}//read_img

void copy_pixels(pixel *original, pixel *newPixel)
{
	int pnum,i,j;
	pnum=0;
	for(i = 0; i < global.h ; i++) {
		for(j = 0; j < global.w ; j++) {
			original[pnum].r = newPixel[pnum].r;
			original[pnum].g = newPixel[pnum].g;
			original[pnum].b = newPixel[pnum].b;
			pnum++;
		}
	}
}

//write_img
void write_img(char *name, pixel *data, int width, int height) {
	FIBITMAP *image;
	RGBQUAD aPixel;
	int i,j;

	image = FreeImage_Allocate(width, height, 24, 0, 0, 0);
	if(!image) {
		perror("FreeImage_Allocate");
		return;
	}
	for(i = 0 ; i < height ; i++) {
		for(j = 0 ; j < width ; j++) {
			aPixel.rgbRed = data[i*width+j].r;
			aPixel.rgbGreen = data[i*width+j].g;
			aPixel.rgbBlue = data[i*width+j].b;

			FreeImage_SetPixelColor(image, j, i, &aPixel);
		}
	}
	if(!FreeImage_Save(FIF_TIFF, image, name, 0)) {
		perror("FreeImage_Save");
	}
	FreeImage_Unload(image);
}//write_img

//write_img
void write_img_png(char *name, pixel *data, int width, int height) {
	FIBITMAP *image;
	RGBQUAD aPixel;
	int i,j;

	image = FreeImage_Allocate(width, height, 24, 0, 0, 0);
	if(!image) {
		perror("FreeImage_Allocate");
		return;
	}
	for(i = 0 ; i < height ; i++) {
		for(j = 0 ; j < width ; j++) {
			aPixel.rgbRed = data[i*width+j].r;
			aPixel.rgbGreen = data[i*width+j].g;
			aPixel.rgbBlue = data[i*width+j].b;

			FreeImage_SetPixelColor(image, j, i, &aPixel);
		}
	}
	if(!FreeImage_Save(FIF_PNG, image, name, 0)) {
		perror("FreeImage_Save");
	}
	FreeImage_Unload(image);
}//write_img

/*draw the image - it is already in the format openGL requires for glDrawPixels*/
void display_image(void)
{
	glDrawPixels(global.w, global.h, GL_RGB, GL_UNSIGNED_BYTE, (GLubyte*)global.data);	
	glFlush();
}//display_image()

// Read the screen image back to the data buffer after drawing to it
void draw_triangle(void)
{
	glDrawPixels(global.w, global.h, GL_RGB, GL_UNSIGNED_BYTE, (GLubyte*)global.workspace);
	glBegin(GL_TRIANGLES);
	glColor3f(1.0,0,0);
	glVertex2i(rand()%global.w,rand()%global.h);
	glColor3f(0,1.0,0);
	glVertex2i(rand()%global.w,rand()%global.h);
	glColor3f(0,0,1.0);
	glVertex2i(rand()%global.w,rand()%global.h);
	glEnd();
	glFlush();
	glReadPixels(0,0,global.w,global.h,GL_RGB, GL_UNSIGNED_BYTE, (GLubyte*)global.workspace);
}

//averages the pixel luminosity among RGB and sets that the same for each pixel
void Greyscale(pixel* working, pixel* data, int width, int height)
{
	int x,y, average;

	for (x=0; x < width; x++)
		for (y=0; y < height; y++){
			average =( working[x+y*width].r + working[x+y*width].g + working[x+y*width].b)/3;
			working[x+y*width].r = average;
			working[x+y*width].g = average;
			working[x+y*width].b = average;
		}

	copy_pixels(data, working);
	glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
}

void OwnFilter1(pixel* working, pixel* data, int width, int height)
{
	int x,y, average;

	for (x=0; x < width; x++)
		for (y=0; y < height; y++){
			average =( (1.587*working[x+y*width].g) + (1.114*working[x+y*width].b) +(1.299 * working[x+y*width].r))/3;
			working[x+y*width].r = average;
			working[x+y*width].g = average;
			working[x+y*width].b = average;
		}

	copy_pixels(data, working);
	glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
}



void AnimatedFilter(pixel* working, pixel* data, int width, int height)
{
	int x,y,i, average;
	char *name = malloc(20);
	for(i = 1; i <= 100; i++)
	{
		for (x=0; x < width; x++)
			for (y=0; y < height; y++){
				working[x+y*width].r = working[x+y*width].r / sin((working[x+y*width].g+100)/i) * cos((working[x+y*width].b*i)/100);
				working[x+y*width].g = working[x+y*width].g / sin((working[x+y*width].b+100)/i) * cos((working[x+y*width].r*i)/100);
				working[x+y*width].b = working[x+y*width].b / sin((working[x+y*width].r+100)/i) * cos((working[x+y*width].g*i)/100);
			}

		copy_pixels(data, working);
		glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
		
		sprintf((char *)name, "GifImage_%d.png", i);
		write_img_png(name, global.data, global.w, global.h);
	}
	free(*name);
}


void OwnFilter2(pixel* working, pixel* data, int width, int height)
{
	int x,y;

	for (x=0; x < width; x++)
		for (y=0; y < height; y++){
			working[x+y*width].r = ((int)pow(working[x+y*width].r,2)) % 255;
			working[x+y*width].g = ((int)pow(working[x+y*width].g,2)) % 255;
			working[x+y*width].b = ((int)pow(working[x+y*width].b,2)) % 255;
		}

	copy_pixels(data, working);
	glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
}

void NTSC(pixel* working, pixel* data, int width, int height)
{
	int x,y,average;

	for (x=0; x < width; x++)
		for (y=0; y < height; y++){
			average =( (0.587*working[x+y*width].g) + (0.114*working[x+y*width].b) +(0.299 * working[x+y*width].r));
			working[x+y*width].r = average;
			working[x+y*width].g = average;
			working[x+y*width].b = average;
		}

	copy_pixels(data, working);
	glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
}

void Monochrome(pixel* working, pixel* data, int width, int height)
{
	int x,y,average;

	for (x=0; x < width; x++)
		for (y=0; y < height; y++){
			average =( (working[x+y*width].g) + (working[x+y*width].b) +(working[x+y*width].r))/3;
			if(average > 128)
			{
				working[x+y*width].r = 255;
				working[x+y*width].g = 255;
				working[x+y*width].b = 255;
			}
			else
			{
				working[x+y*width].r = 0;
				working[x+y*width].g = 0;
				working[x+y*width].b = 0;
			}
		}

	copy_pixels(data, working);
	glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
}

void ChannelSwap(pixel* working, pixel* data, int width, int height)
{
	int x,y,r;

	for (x=0; x < width; x++)
		for (y=0; y < height; y++){
			r = working[x+y*width].r;
			working[x+y*width].r = working[x+y*width].g;
			working[x+y*width].g = working[x+y*width].b;
			working[x+y*width].b = r;
		}

	copy_pixels(data, working);
	glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
}

void RGB_isolate(pixel* working, pixel* data, int width, int height, char colour)
{
	int x,y,r;

	for (x=0; x < width; x++)
		for (y=0; y < height; y++){
			switch(colour)
			{
				case 'r':
				case 'R':
					working[x+y*width].g = 0;
					working[x+y*width].b = 0;
					break;
				case 'g':
				case 'G':
					working[x+y*width].r = 0;
					working[x+y*width].b = 0;
					break;
				case 'b':
				case 'B':
					working[x+y*width].r = 0;
					working[x+y*width].g = 0;
					break;
			}
		}

	copy_pixels(data, working);
	glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
}

void Max(pixel* working, pixel* data, int width, int height)
{
	int x,y;

	for (x=0; x < width; x++)
		for (y=0; y < height; y++){
			working[x+y*width].r = getMaxColour(data, x, y, 'R');
			working[x+y*width].g = getMaxColour(data, x, y, 'G');
			working[x+y*width].b = getMaxColour(data, x, y, 'B');
		}

	copy_pixels(data, working);
	glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
}

void RGB_intensify(pixel* working, pixel* data, int width, int height, char colour)
{
	int x,y,r;

	for (x=0; x < width; x++)
		for (y=0; y < height; y++){
			switch(colour)
			{
				case 'r':
				case 'R':
					if(working[x+y*width].r*1.05 == working[x+y*width].r || working[x+y*width].r < 10)
						working[x+y*width].r += 5;
					else if(working[x+y*width].r*1.05 < 255)
						working[x+y*width].r = working[x+y*width].r*1.05;
					else
						working[x+y*width].r = 255;
					break;
				case 'g':
				case 'G':
					if(working[x+y*width].g*1.05 == working[x+y*width].g || working[x+y*width].g < 10)
						working[x+y*width].g += 5;
					else if(working[x+y*width].g*1.05 < 255)
						working[x+y*width].g = working[x+y*width].g*1.05;
					else
						working[x+y*width].g = 255;
					break;
				case 'b':
				case 'B':
					if(working[x+y*width].b*1.05 == working[x+y*width].b  || working[x+y*width].b < 10)
						working[x+y*width].b += 5;
					else if(working[x+y*width].b*1.05 < 255)
						working[x+y*width].b = working[x+y*width].b*1.05;
					else
						working[x+y*width].b = 255;
					break;
			}
		}

	copy_pixels(data, working);
	glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
}

int getMaxColour(pixel* data, int x, int y, char channel)
{
	int max, i, j;
	max = 0;
	for(i = -1; i <= 1; i++)
		for(j = -1; j <= 1; j++)
		{
			if((x+i) < 0 || (x+i) >= global.w || (y+j) < 0 || (y+j) >= global.h) continue;
			switch(channel)
			{
				case 'R':
				case 'r':
					max = max < data[(x+i)+(y+j)*global.w].r ? data[(x+i)+(y+j)*global.w].r : max;
				case 'G':
				case 'g':
					max = max < data[(x+i)+(y+j)*global.w].g ? data[(x+i)+(y+j)*global.w].g : max;
				case 'B':
				case 'b':
					max = max < data[(x+i)+(y+j)*global.w].b ? data[(x+i)+(y+j)*global.w].b : max;
			}
		}

	return max;
}

void Min(pixel* working, pixel* data, int width, int height)
{
	int x,y;

	for (x=0; x < width; x++)
		for (y=0; y < height; y++){
			working[x+y*width].r = getMinColour(data, x, y, 'R');
			working[x+y*width].g = getMinColour(data, x, y, 'G');
			working[x+y*width].b = getMinColour(data, x, y, 'B');
		}

	copy_pixels(data, working);
	glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
}

int getMinColour(pixel* data, int x, int y, char channel)
{
	int min, i, j;
	min = 255;
	for(i = -1; i <= 1; i++)
		for(j = -1; j <= 1; j++)
		{
			if((x+i) < 0 || (x+i) >= global.w || (y+j) < 0 || (y+j) >= global.h) continue;
			switch(channel)
			{
				case 'R':
				case 'r':
					min = min > data[(x+i)+(y+j)*global.w].r ? data[(x+i)+(y+j)*global.w].r : min;
				case 'G':
				case 'g':
					min = min > data[(x+i)+(y+j)*global.w].g ? data[(x+i)+(y+j)*global.w].g : min;
				case 'B':
				case 'b':
					min = min > data[(x+i)+(y+j)*global.w].b ? data[(x+i)+(y+j)*global.w].b : min;
			}
		}

	return min;
}

void Average(pixel* working, pixel* data, int width, int height)
{
	int x,y;

	for (x=0; x < width; x++)
		for (y=0; y < height; y++){
			working[x+y*width].r = getCovolutionFilterColour(data, x, y, 'R', global.average);
			working[x+y*width].g = getCovolutionFilterColour(data, x, y, 'G', global.average);
			working[x+y*width].b = getCovolutionFilterColour(data, x, y, 'B', global.average);
		}

	copy_pixels(data, working);
	glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
}

void HorSobel(pixel* working, pixel* data, int width, int height)
{
	int x,y;

	for (x=0; x < width; x++)
		for (y=0; y < height; y++){
			working[x+y*width].r = getCovolutionFilterColour(data, x, y, 'R', global.sobelHor);
			working[x+y*width].g = getCovolutionFilterColour(data, x, y, 'G', global.sobelHor);
			working[x+y*width].b = getCovolutionFilterColour(data, x, y, 'B', global.sobelHor);
		}

	copy_pixels(data, working);
	glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
}

void VertSobel(pixel* working, pixel* data, int width, int height)
{
	int x,y;

	for (x=0; x < width; x++)
		for (y=0; y < height; y++){
			working[x+y*width].r = getCovolutionFilterColour(data, x, y, 'R', global.sobelVert);
			working[x+y*width].g = getCovolutionFilterColour(data, x, y, 'G', global.sobelVert);
			working[x+y*width].b = getCovolutionFilterColour(data, x, y, 'B', global.sobelVert);
		}

	copy_pixels(data, working);
	glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
}

void OwnMatrix1Filter(pixel* working, pixel* data, int width, int height)
{
	int x,y;

	for (x=0; x < width; x++)
		for (y=0; y < height; y++){
			working[x+y*width].r = getCovolutionFilterColour(data, x, y, 'R', global.OwnMatrix1);
			working[x+y*width].g = getCovolutionFilterColour(data, x, y, 'G', global.OwnMatrix1);
			working[x+y*width].b = getCovolutionFilterColour(data, x, y, 'B', global.OwnMatrix1);
		}

	copy_pixels(data, working);
	glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
}

void OwnMatrix2Filter(pixel* working, pixel* data, int width, int height)
{
	int x,y;

	for (x=0; x < width; x++)
		for (y=0; y < height; y++){
			working[x+y*width].r = getCovolutionFilterColour(data, x, y, 'R', global.OwnMatrix2);
			working[x+y*width].g = getCovolutionFilterColour(data, x, y, 'G', global.OwnMatrix2);
			working[x+y*width].b = getCovolutionFilterColour(data, x, y, 'B', global.OwnMatrix2);
		}

	copy_pixels(data, working);
	glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
}

void Sobel(pixel* working, pixel* data, int width, int height)
{
	int x,y;

	for (x=0; x < width; x++)
		for (y=0; y < height; y++){
			working[x+y*width].r = sqrt(pow(getCovolutionFilterColour(data, x, y, 'R', global.sobelVert),2)+pow(getCovolutionFilterColour(data, x, y, 'R', global.sobelHor),2));
			working[x+y*width].g = sqrt(pow(getCovolutionFilterColour(data, x, y, 'G', global.sobelVert),2)+pow(getCovolutionFilterColour(data, x, y, 'G', global.sobelHor),2));
			working[x+y*width].b = sqrt(pow(getCovolutionFilterColour(data, x, y, 'B', global.sobelVert),2)+pow(getCovolutionFilterColour(data, x, y, 'B', global.sobelHor),2));
		}

	copy_pixels(data, working);
	glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
}

int getCovolutionFilterColour(pixel* data, int x, int y, char channel, int **filter)
{
	int i, j, sum, total;
	sum = 0;
	total = 0;
	for(i = -1; i <= 1; i++)
		for(j = -1; j <= 1; j++)
		{
			if((x+i) < 0 || (x+i) >= global.w || (y+j) < 0 || (y+j) >= global.h) continue;
			sum += filter[i+1][j+1];
			switch(channel)
			{
				case 'R':
				case 'r':
					total += (data[(x+i)+(y+j)*global.w].r * filter[i+1][j+1]);
					break;
				case 'G':
				case 'g':
					total += (data[(x+i)+(y+j)*global.w].g * filter[i+1][j+1]);
					break;
				case 'B':
				case 'b':
					total += (data[(x+i)+(y+j)*global.w].b * filter[i+1][j+1]);
					break;
			}
		}

	if(sum == 0) sum = 1;
	total = total/sum;
	if(total > 255) total = 255;
	if(total < 0) total = 0;
	return total;
}

void setPixelQunatizeColor(pixel* data, pixel* work, int x, int y, int **colours, int numOfColours)
{
	int min,r,g,b,count, value;
	r = data[(x)+(y)*global.w].r;
	g = data[(x)+(y)*global.w].g;
	b = data[(x)+(y)*global.w].b;

	min = (pow(colours[0][0] - data[(x)+(y)*global.w].r,2)+
			       pow(colours[0][1] - data[(x)+(y)*global.w].g,2)+
				   pow(colours[0][2] - data[(x)+(y)*global.w].b,2));
	for(count = 0; count < numOfColours; count++)
	{
		value = (pow(colours[count][0] - data[(x)+(y)*global.w].r,2)+
			       pow(colours[count][1] - data[(x)+(y)*global.w].g,2)+
				   pow(colours[count][2] - data[(x)+(y)*global.w].b,2));
		if(min > value)
		{
			min = value;
			r = colours[count][0];
			g = colours[count][1];
			b = colours[count][2];
		}
	}

	work[(x)+(y)*global.w].r = r;
	work[(x)+(y)*global.w].g = g;
	work[(x)+(y)*global.w].b = b;
}

void RandomColour(pixel* working, pixel* data, int width, int height)
{
	int x,y;

	for (x=0; x < width; x++)
		for (y=0; y < height; y++){
			setPixelQunatizeColor(data, working, x, y, global.Random, 8);
		}

	copy_pixels(data, working);
	glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
}

void NotRandomColour(pixel* working, pixel* data, int width, int height)
{
	int x,y;

	for (x=0; x < width; x++)
		for (y=0; y < height; y++){
			setPixelQunatizeColor(data, working, x, y, global.PresetColours, 8);
		}

	copy_pixels(data, working);
	glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
}

void Paint(int x, int y)
{
	int j,k;
	int brushWidth = (global.paintBrushSize/2);
	for(j = -brushWidth; j <= brushWidth; j++)
		for(k = -brushWidth; k <= brushWidth; k++)
		{
			if((x+j) < 0 || (x+j) >= global.w || (y+k) < 0 || (y+k) >= global.h) continue;
			global.workspace[(x+j)+(y+k)*global.w].r = global.PresetColours[global.paintColour][0];
			global.workspace[(x+j)+(y+k)*global.w].g = global.PresetColours[global.paintColour][1];
			global.workspace[(x+j)+(y+k)*global.w].b = global.PresetColours[global.paintColour][2];
		}
		copy_pixels(global.data, global.workspace);
	glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
}

void Blend1(int x, int y)
{
	global.workspace[x+y*global.w].r = (global.data[x+y*global.w].r + global.PresetColours[global.paintColour][0]) / 2;
	global.workspace[x+y*global.w].g = (global.data[x+y*global.w].g + global.PresetColours[global.paintColour][1]) / 2;
	global.workspace[x+y*global.w].b = (global.data[x+y*global.w].b + global.PresetColours[global.paintColour][2]) / 2;

	copy_pixels(global.data, global.workspace);
	glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
}

void Blend2(int x, int y)
{
	if(global.workspace[x+y*global.w].r > global.workspace[x+y*global.w].g && 
		global.workspace[x+y*global.w].r > global.workspace[x+y*global.w].b)
	{
		global.workspace[x+y*global.w].r = 255;
		global.workspace[x+y*global.w].g = 0;
		global.workspace[x+y*global.w].b = 0;
	}
	else if(global.workspace[x+y*global.w].g > global.workspace[x+y*global.w].r && 
		global.workspace[x+y*global.w].g > global.workspace[x+y*global.w].b)
	{
		global.workspace[x+y*global.w].g = 255;
		global.workspace[x+y*global.w].r = 0;
		global.workspace[x+y*global.w].b = 0;
	}
	else if(global.workspace[x+y*global.w].b > global.workspace[x+y*global.w].r && 
		global.workspace[x+y*global.w].b > global.workspace[x+y*global.w].g)
	{
		global.workspace[x+y*global.w].b = 255;
		global.workspace[x+y*global.w].g = 0;
		global.workspace[x+y*global.w].r = 0;
	}
	else
	{
		global.workspace[x+y*global.w].b = 0;
		global.workspace[x+y*global.w].g = 0;
		global.workspace[x+y*global.w].r = 0;
	}
	copy_pixels(global.data, global.workspace);
	glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
}

//set the writing space back to black
void Clear(pixel* working, pixel* data, int width, int height)
{
	int x,y, average;

	for (x=0; x < width; x++)
		for (y=0; y < height; y++){
			working[x+y*width].r = 0;
			working[x+y*width].g = 0;
			working[x+y*width].b = 0;
		}

	copy_pixels(data, working);
	glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
}

void TogglePaintBrushSize()
{
	if(global.paintBrushSize > 9)
		global.paintBrushSize = 1;
	else
		global.paintBrushSize += 2;
}

void ChangePaintColour(int colour)
{
	if(colour > 9 || colour < 0) return;
	global.paintColour = colour;
}

void ChooseRandomColour()
{
	global.PresetColours[10][0] = rand() % 256;
	global.PresetColours[10][1] = rand() % 256;
	global.PresetColours[10][1] = rand() % 256;
	global.paintColour = 10;
}

void ChooseCurrentPixelColour()
{
	global.PresetColours[10][0] = global.data[global.mouse_x+global.mouse_y*global.w].r;	
	global.PresetColours[10][1] = global.data[global.mouse_x+global.mouse_y*global.w].g;
	global.PresetColours[10][2] = global.data[global.mouse_x+global.mouse_y*global.w].b;
	global.paintColour = 10;
}

/* A simple thresholding filter.
*/
void MyFilter(pixel* Im, int myIm_Width, int myIm_Height){
	int x,y;

	for (x=0; x < myIm_Width; x++)
		for (y=0; y < myIm_Height; y++){
			if (Im[x+y*myIm_Width].b > 128)
				Im[x+y*myIm_Width].b = 255;
			else 
				Im[x+y*myIm_Width].b = 0;

			if (Im[x+y*myIm_Width].g > 128)
				Im[x+y*myIm_Width].g = 255;
			else 
				Im[x+y*myIm_Width].g = 0;

			if (Im[x+y*myIm_Width].r > 128)
				Im[x+y*myIm_Width].r = 255;
			else 
				Im[x+y*myIm_Width].r = 0;
		}

		copy_pixels(global.data, Im);
	glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
}//My_Filter

void drawLine(int startx, int starty, int endx, int endy)
{
	int dy, dx;

	double m, y, x;
	dy = endy-starty; 
	dx = endx-startx;
	m = (double)dy / dx;

	//swap starts and ends
	if(((m < 1 && m > -1) && startx > endx) || (m > 1 || m < -1) && starty > endy)
	{
			dx = startx;
			startx = endx;
			endx = dx;

			dy = starty;
			starty = endy;
			endy = dy;

			dy = endy-starty; 
			dx = endx-startx;
			m = (double)dy / dx;
		
	}

	if(m < 1 && m > -1)
	{
		y = starty;
		for (x=startx; x < endx; x++) {
			dy = y;
		   Paint(x, dy);
		   y = y + m;
		}
	}
	else
	{
		m = (double)dx / dy;
		x = startx;
		for (y=starty; y < endy; y++) {
			dx = x;
		   Paint(dx, y);
		   x = x + m;
		}
	}
}

void deAllocateGlobalArrays()
{
	int x;

	/* allocate each row's columns */
    for( x = 0; x < 3; x++ )
	{
        free(global.average[x]);
        free(global.sobelHor[x]);
        free(global.sobelVert[x]);
		free(global.OwnMatrix1[x]);
		free(global.OwnMatrix2[x]);
	}

	/* allocate each row's columns */
    for( x = 0; x < 11; x++ )
	{
		free(global.PresetColours[x]);
		if(x > 7) continue;
		free(global.Random[x]);
	}

	free(global.average);
	free(global.sobelHor);
	free(global.sobelVert);
	free(global.OwnMatrix1);
	free(global.OwnMatrix2);
	free(global.Random);
	free(global.PresetColours);
}

void togglePaint()
{
	global.paintOn = !global.paintOn;
	global.blend1On = false;
	global.blend2On = false;
	global.pete1 = false;
	global.pete2 = false;
}

void toggleBlend1()
{
	global.paintOn = false;
	global.blend1On = !global.blend1On;
	global.blend2On = false;
	global.pete1 = false;
	global.pete2 = false;
}

void toggleBlend2()
{
	global.paintOn = false;
	global.blend1On = false;
	global.blend2On = !global.blend2On;
	global.pete1 = false;
	global.pete2 = false;
}

void togglePete1()
{
	global.paintOn = false;
	global.blend1On = false;
	global.blend2On = false;
	global.pete2 = false;
	global.pete1 = !global.pete1;
}
void togglePete2()
{
	global.paintOn = false;
	global.blend1On = false;
	global.blend2On = false;
	global.pete1 = false;
	global.pete2 = !global.pete2;
}

void Pete1(int x, int y)
{
	int j,k;
	int brushWidth = (global.paintBrushSize/2);
	if(global.paintColour >= 9) global.paintColour = 0;
	else global.paintColour++;
	for(j = -brushWidth; j <= brushWidth; j++)
		for(k = -brushWidth; k <= brushWidth; k++)
		{
			if((x+j) < 0 || (x+j) >= global.w || (y+k) < 0 || (y+k) >= global.h) continue;
			global.workspace[(x+j)+(y+k)*global.w].r = global.PresetColours[global.paintColour][0];
			global.workspace[(x+j)+(y+k)*global.w].g = global.PresetColours[global.paintColour][1];
			global.workspace[(x+j)+(y+k)*global.w].b = global.PresetColours[global.paintColour][2];
		}
		copy_pixels(global.data, global.workspace);
	glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
}

void Pete2(int x, int y)
{
	int j,k, average;
	int brushWidth = (global.paintBrushSize/2);
	for(j = -brushWidth; j <= brushWidth; j++)
		for(k = -brushWidth; k <= brushWidth; k++)
		{
			if((x+j) < 0 || (x+j) >= global.w || (y+k) < 0 || (y+k) >= global.h) continue;
			average = (global.workspace[(x+j)+(y+k)*global.w].r + global.workspace[(x+j)+(y+k)*global.w].g + global.workspace[(x+j)+(y+k)*global.w].b) / 3;
			global.workspace[(x+j)+(y+k)*global.w].r = average;
			global.workspace[(x+j)+(y+k)*global.w].g = average;
			global.workspace[(x+j)+(y+k)*global.w].b = average;
		}
		copy_pixels(global.data, global.workspace);
	glutPostRedisplay();	// Tell glut that the image has been updated and needs to be redrawn
}

void drawRectangle(int startx, int starty, int endx, int endy)
{
	drawLine(startx, starty, endx, starty);
	drawLine(endx, starty, endx, endy);
	drawLine(endx, endy, startx, endy);
	drawLine(startx, endy, startx, starty);
}

/*glut keyboard function*/
void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 0x1B:
	case'q':
	case 'Q':
		free(global.data);
		free(global.workspace);
		free(global.original);
		deAllocateGlobalArrays();
		exit(0);
		break;
	case'r':
	case 'R':
		copy_pixels(global.data, global.original);
		copy_pixels(global.workspace, global.original);
		display_image();
		break;
	case's':
	case'S':
		printf("SAVING IMAGE: backup.tif\n");
		write_img("backup.tif", global.data, global.w, global.h);
		break;
	case 'c':
	case 'C':
		Clear(global.workspace, global.data, global.w, global.h);
		break;	
	case '+':
		TogglePaintBrushSize();
		break;
	case '0':
			ChangePaintColour(0);
	case '1':
			ChangePaintColour(1);
	case '2':
			ChangePaintColour(2);
	case '3':
			ChangePaintColour(3);
	case '4':
			ChangePaintColour(4);
	case '5':
			ChangePaintColour(5);
	case '6':
			ChangePaintColour(6);
	case '7':
			ChangePaintColour(7);
	case '8':
			ChangePaintColour(8);
	case '9':
			ChangePaintColour(9);
	case 'x':
	case 'X':
		ChooseRandomColour();
		break;	
	case 'v':
	case 'V':
		ChooseCurrentPixelColour();
		break;	
	case ' ':
		togglePaint();
		break;
	case 'l':
	case 'L':
		if(global.startX == 0 && global.startY == 0)
		{
			global.startX = global.mouse_x;
			global.startY = global.mouse_y;
		}
		else
		{
			drawLine(global.startX, global.startY, global.mouse_x, global.mouse_y);
			global.startX = 0;
			global.startY = 0;
		}
		break;
	case 'b':
	case 'B':
		if(global.startX == 0 && global.startY == 0)
		{
			global.startX = global.mouse_x;
			global.startY = global.mouse_y;
		}
		else
		{
			drawRectangle(global.startX, global.startY, global.mouse_x, global.mouse_y);
			global.startX = 0;
			global.startY = 0;
		}
		break;
		
	case 'w':
	case 'W':
		toggleBlend1();
		break;
	case 'e':
	case 'E':
		toggleBlend2();
		break;
		break;
	case 'o':
	case 'O':
		togglePete1();
		break;
		break;
	case 'p':
	case 'P':
		togglePete2();
		break;

	}
}//keyboard

void fillGlobalArrays()
{
	int x;
	
	global.average = malloc ( sizeof ( int * ) * 3 );
	global.sobelHor = malloc ( sizeof ( int * ) * 3 );
	global.sobelVert = malloc ( sizeof ( int * ) * 3 );
	global.OwnMatrix1 = malloc ( sizeof ( int * ) * 3 );
	global.OwnMatrix2 = malloc ( sizeof ( int * ) * 3 );
	global.Random = malloc ( sizeof ( int * ) * 8 );
	global.PresetColours = malloc ( sizeof ( int * ) * 11 );

	/* allocate each row's columns */
    for( x = 0; x < 3; x++ )
	{
        global.average[x] = malloc( sizeof( int ) * 3 );
        global.sobelHor[x] = malloc( sizeof( int ) * 3 );
        global.sobelVert[x] = malloc( sizeof( int ) * 3 );
		global.OwnMatrix1[x] = malloc( sizeof( int ) * 3 );
		global.OwnMatrix2[x] = malloc( sizeof( int ) * 3 );
	}

	/* allocate each row's columns */
    for( x = 0; x < 11; x++ )
	{		
		global.PresetColours[x] = malloc( sizeof( int ) * 3 );

		if(x >= 8) continue;
        global.Random[x] = malloc( sizeof( int ) * 3 );
	}

	//average
	global.average[0][0] = 1;
	global.average[0][1] = 1;
	global.average[0][2] = 1;
	global.average[1][0] = 1;
	global.average[1][1] = 1;
	global.average[1][2] = 1;
	global.average[2][0] = 1;
	global.average[2][1] = 1;
	global.average[2][2] = 1;

	//sobelVert
	global.sobelVert[0][0] = 1;
	global.sobelVert[0][1] = 0;
	global.sobelVert[0][2] = -1;
	global.sobelVert[1][0] = 2;
	global.sobelVert[1][1] = 0;
	global.sobelVert[1][2] = -2;
	global.sobelVert[2][0] = 1;
	global.sobelVert[2][1] = 0;
	global.sobelVert[2][2] = -1;

	//sobelHor
	global.sobelHor[0][0] = 1;
	global.sobelHor[0][1] = 2;
	global.sobelHor[0][2] = 1;
	global.sobelHor[1][0] = 0;
	global.sobelHor[1][1] = 0;
	global.sobelHor[1][2] = 0;
	global.sobelHor[2][0] = -1;
	global.sobelHor[2][1] = -2;
	global.sobelHor[2][2] = -1;

	
	//ownMatrix1
	global.OwnMatrix1[0][0] = -2;
	global.OwnMatrix1[0][1] = -1;
	global.OwnMatrix1[0][2] = 0;
	global.OwnMatrix1[1][0] = -1;
	global.OwnMatrix1[1][1] = 1;
	global.OwnMatrix1[1][2] = 1;
	global.OwnMatrix1[2][0] = 0;
	global.OwnMatrix1[2][1] = 1;
	global.OwnMatrix1[2][2] = 2;

	//ownMatrix2
	global.OwnMatrix2[0][0] = 1;
	global.OwnMatrix2[0][1] = 1;
	global.OwnMatrix2[0][2] = 1;
	global.OwnMatrix2[1][0] = 1;
	global.OwnMatrix2[1][1] = -7;
	global.OwnMatrix2[1][2] = 1;
	global.OwnMatrix2[2][0] = 1;
	global.OwnMatrix2[2][1] = 1;
	global.OwnMatrix2[2][2] = 1;

	//Random
	global.Random[0][0] = rand() % 256;
	global.Random[0][1] = rand() % 256;
	global.Random[0][2] = rand() % 256;
	global.Random[1][0] = rand() % 256;
	global.Random[1][1] = rand() % 256;
	global.Random[1][2] = rand() % 256;
	global.Random[2][0] = rand() % 256;
	global.Random[2][1] = rand() % 256;
	global.Random[2][2] = rand() % 256;
	global.Random[3][0] = rand() % 256;
	global.Random[3][1] = rand() % 256;
	global.Random[3][2] = rand() % 256;
	global.Random[4][0] = rand() % 256;
	global.Random[4][1] = rand() % 256;
	global.Random[4][2] = rand() % 256;
	global.Random[5][0] = rand() % 256;
	global.Random[5][1] = rand() % 256;
	global.Random[5][2] = rand() % 256;
	global.Random[6][0] = rand() % 256;
	global.Random[6][1] = rand() % 256;
	global.Random[6][2] = rand() % 256;
	global.Random[7][0] = rand() % 256;
	global.Random[7][1] = rand() % 256;
	global.Random[7][2] = rand() % 256;

	//NotRandom
	global.PresetColours[0][0] = 255; //RED
	global.PresetColours[0][1] = 0;
	global.PresetColours[0][2] = 0;
	global.PresetColours[1][0] = 0; //green
	global.PresetColours[1][1] = 255;
	global.PresetColours[1][2] = 0;
	global.PresetColours[2][0] = 0; //blue
	global.PresetColours[2][1] = 0;
	global.PresetColours[2][2] = 255;
	global.PresetColours[3][0] = 0; //black
	global.PresetColours[3][1] = 0;
	global.PresetColours[3][2] = 0;
	global.PresetColours[4][0] = 255;//white
	global.PresetColours[4][1] = 255;
	global.PresetColours[4][2] = 255;
	global.PresetColours[5][0] = 127;//grey
	global.PresetColours[5][1] = 127;
	global.PresetColours[5][2] = 127;
	global.PresetColours[6][0] = 255;//pink
	global.PresetColours[6][1] = 0;
	global.PresetColours[6][2] = 255;
	global.PresetColours[7][0] = 255;//pumpkin orange
	global.PresetColours[7][1] = 153;
	global.PresetColours[7][2] = 0;
	global.PresetColours[8][0] = 102;//baby blue
	global.PresetColours[8][1] = 255;
	global.PresetColours[8][2] = 255;
	global.PresetColours[9][0] = 255;//yellow
	global.PresetColours[9][1] = 255;
	global.PresetColours[9][2] = 0;
	global.PresetColours[10][0] = 0;//random
	global.PresetColours[10][1] = 0;
	global.PresetColours[10][2] = 0;
}

void getMousePosition(int x, int y)
{

	global.mouse_x = x;
	global.mouse_y = global.h-y;

	if(global.paintOn)
	{
		Paint(global.mouse_x, global.mouse_y);
	}
	else if(global.blend1On)
	{
		Blend1(global.mouse_x, global.mouse_y);
	}
	else if(global.blend2On)
	{
		Blend2(global.mouse_x, global.mouse_y);
	}
	else if(global.pete1)
	{
		Pete1(global.mouse_x, global.mouse_y);
	}
	else if(global.pete2)
	{
		Pete2(global.mouse_x, global.mouse_y);
	}

}

void menuItems(int num)
{
	switch(num)
	{
		case 0:
			free(global.data);
			free(global.workspace);
			free(global.original);
			deAllocateGlobalArrays();
			exit(0);
			break;
		case 1:
			copy_pixels(global.data, global.original);
			copy_pixels(global.workspace, global.original);
			display_image();
			break;
		case 7:
			RGB_isolate(global.workspace, global.data, global.w, global.h,'R');
			break;
		case 8:
			RGB_isolate(global.workspace, global.data, global.w, global.h,'G');
			break;
		case 9:
			RGB_isolate(global.workspace, global.data, global.w, global.h,'B');
			break;
		case 10:
			Max(global.workspace, global.data, global.w, global.h);
			break;
		case 11:
			Min(global.workspace, global.data, global.w, global.h);
			break;
		case 12:
			RGB_intensify(global.workspace, global.data, global.w, global.h, 'R');
			break;
		case 13:
			RGB_intensify(global.workspace, global.data, global.w, global.h, 'G');
			break;
		case 14:
			RGB_intensify(global.workspace, global.data, global.w, global.h, 'B');
			break;		
		case 15:
			Average(global.workspace, global.data, global.w, global.h);
			break;			
		case 18:
			Sobel(global.workspace, global.data, global.w, global.h);
			break;			
		case 16:
			HorSobel(global.workspace, global.data, global.w, global.h);
			break;			
		case 17:
			VertSobel(global.workspace, global.data, global.w, global.h);
			break;	
		case 3:
			Greyscale(global.workspace, global.data, global.w, global.h);
			break;
		case 4:
			NTSC(global.workspace, global.data, global.w, global.h);
			break;		
		case 5:
			Monochrome(global.workspace, global.data, global.w, global.h);
			break;	
		case 6:
			ChannelSwap(global.workspace, global.data, global.w, global.h);
			break;
		case 2:
			printf("SAVING IMAGE: backup.tif\n");
			write_img("backup.tif", global.data, global.w, global.h);
			break;
		case 21:
			RandomColour(global.workspace, global.data, global.w, global.h);
			break;
		case 22:
			NotRandomColour(global.workspace, global.data, global.w, global.h);
			break;
		case 23:
			OwnFilter1(global.workspace, global.data, global.w, global.h);
			OwnFilter1(global.workspace, global.data, global.w, global.h);
			break;
		case 24:
			OwnFilter2(global.workspace, global.data, global.w, global.h);
			break;
		case 19:
			OwnMatrix1Filter(global.workspace, global.data, global.w, global.h);
			break;
		case 20:
			OwnMatrix2Filter(global.workspace, global.data, global.w, global.h);
			break;
		case 25:
			AnimatedFilter(global.workspace, global.data, global.w, global.h);
			break;
	}
}

void createMenu()
{
	int filter_submenu_id, display_id, basic_id, 
		convolution_id, quantize_id, my_filter_id, menu_id;

	basic_id = glutCreateMenu(menuItems);
    glutAddMenuEntry("Max", 10);
    glutAddMenuEntry("Min", 11);
    glutAddMenuEntry("Red Intensify", 12);
    glutAddMenuEntry("Green Intensify", 13); 
    glutAddMenuEntry("Blue Intensify", 14); 

	convolution_id = glutCreateMenu(menuItems);
    glutAddMenuEntry("Average", 15);
    glutAddMenuEntry("Hor Sobel", 16);
    glutAddMenuEntry("Vert Sobel", 17);
    glutAddMenuEntry("Sobel", 18); 
    glutAddMenuEntry("Peter 1", 19);
    glutAddMenuEntry("Peter 2", 20); 

	quantize_id = glutCreateMenu(menuItems);
    glutAddMenuEntry("Random Colour", 21);
    glutAddMenuEntry("Pre-set Colour", 22);

	my_filter_id = glutCreateMenu(menuItems);
    glutAddMenuEntry("Filter 1", 23);
    glutAddMenuEntry("Filter 2", 24);
    glutAddMenuEntry("Animated filter", 25);

	display_id = glutCreateMenu(menuItems);
    glutAddMenuEntry("Grey", 3);
    glutAddMenuEntry("NTSC", 4);
    glutAddMenuEntry("Monochrome", 5);
    glutAddMenuEntry("Channel Swap", 6); 
    glutAddMenuEntry("Pure Red", 7); 
    glutAddMenuEntry("Pure Green", 8); 
    glutAddMenuEntry("Pure Blue", 9); 

	filter_submenu_id = glutCreateMenu(menuItems);
    glutAddSubMenu("Display", display_id);
    glutAddSubMenu("Basic Filter", basic_id);
    glutAddSubMenu("Convolution", convolution_id);
    glutAddSubMenu("Quantize", quantize_id);  
    glutAddSubMenu("My Filter", my_filter_id);  

	menu_id = glutCreateMenu(menuItems);
    glutAddMenuEntry("Reset/Default", 1);
    glutAddMenuEntry("Quit", 0);     
    glutAddMenuEntry("Save", 2);
    glutAddSubMenu("Filters", filter_submenu_id);
	glutAttachMenu(GLUT_RIGHT_BUTTON);

}

int main(int argc, char** argv)
{
	global.data = read_img("img.tif", &global.w, &global.h);
	global.original = read_img("img.tif", &global.w, &global.h);
	global.workspace = read_img("img.tif", &global.w, &global.h);
	global.paintBrushSize = 1;
	global.paintColour = 0;
	global.mouse_x = 0;
	global.mouse_y = 0;
	global.startX = 0;
	global.startY = 0;
	global.paintOn = false;
	fillGlobalArrays();

	if (global.data==NULL)
	{
		printf("Error loading image file img.tif\n");
		return 1;
	}
	printf("Q:quit\nR:Reset/Default\nS:save\nc:Clear\n\n");
	printf("0-9:Change the Colour\n");
	printf("Space:Toggle Paint\nw:Toggle Blend 1\ne:Toggle Blend 2\no:Toggle Pete Paint 1\np:Toggle Pete Paint 2\n");
	printf("+:Toggle Brush Size\nx:Random Colour\nv:Choose Current Pixel\n");
	printf("L:Draw Line (first click for start, second click for end)\n");
	printf("B:Draw Rectangle (first click for corner, second click for opposite corner)\n");
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_RGB|GLUT_SINGLE);
	glutInitWindowSize(global.w,global.h);
	glutCreateWindow("SIMPLE DISPLAY");
	glShadeModel(GL_SMOOTH);
	// put all the menu functions in one nice procedure
	createMenu();
	glutDisplayFunc(display_image);
	glutKeyboardFunc(keyboard);
	glutPassiveMotionFunc( getMousePosition );
	glMatrixMode(GL_PROJECTION);
	glOrtho(0,global.w,0,global.h,0,1);
	
	glutMainLoop();

	return 0;
}
